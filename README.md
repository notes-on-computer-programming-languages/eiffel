# eiffel

Object-oriented language. https://en.m.wikipedia.org/wiki/Eiffel_(programming_language)

## Language
* [Documentation](https://www.eiffel.org/documentation)
* [*Using libraries*](https://www.eiffel.org/doc/eiffelstudio/Using_libraries)
* [*Memory management*](https://www.eiffel.org/doc/eiffel/ET-_The_Dynamic_Structure-_Execution_Model#Memory_management)
* [functional programming](https://www.eiffel.org/taxonomy/term/121) (tag)
* [*Eiffel as a functional language*](https://www.eiffel.org/doc-file/eiffel/expression_language.pdf)
  Bertrand Meyer 2012-2014

### Related languages
* Lisaac (on [Debian](https://packages.debian.org/en/lisaac) but old)
  * [lisaac](https://google.com/search?q=lisaac)

## Libraries repository
* [Eiffel Libraries](https://www.eiffel.org/resources/libraries)
* [IRON package repository](https://iron.eiffel.com/repository/)

## Some libraries
* [The EiffelWeb Framework (EWF)](https://www.eiffel.org/doc/solutions/EiffelWeb_framework)
* [EiffelStore ODBC with PostgreSQL](https://www.eiffel.org/doc/solutions/EiffelStore_ODBC_with_PostgreSQL)

## Implementations
### Eiffel Studio
* [EiffelStudio Integrated Development Environment
  ](https://dev.eiffel.com/)
* [Eiffel Sites and Links](https://dev.eiffel.com/Eiffel_Sites_and_Links)
* [Compiling Hello World](https://dev.eiffel.com/Compiling_Hello_World)
* [EiffelStudio: Using command line options](https://www.eiffel.org/doc/eiffelstudio/EiffelStudio-_Using_command_line_options)
* [Docker image](https://hub.docker.com/r/eiffel/eiffel)

### LibertyEiffel
* https://www.liberty-eiffel.org/
* [Debian packages](http://apt.liberty-eiffel.org/)

## Books
### Eiffel Object-Oriented Programming
* 1995, 2015?
* A. J. Tyrrell
* https://link.springer.com/book/10.1007/978-1-349-13875-3
* http://www.worldcat.org/oclc/604360768